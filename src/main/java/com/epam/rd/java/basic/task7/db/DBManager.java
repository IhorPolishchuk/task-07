package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private final String APP_PROPS_FILE = "app.properties";

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null)
			instance = new DBManager();
		return instance;
	}

	private DBManager() {}

	private Properties getProps() throws DBException {
		Properties props = new Properties();
		try (FileInputStream fis = new FileInputStream(APP_PROPS_FILE))
		{
			props.load(fis);
		}
		catch (IOException e) { throw new DBException("Properties file not found", e); }
		return props;
	}

	public List<User> findAllUsers() throws DBException {
		String query = "SELECT * FROM users";
		List<User> result = new ArrayList<>();

		try (Connection connection = DriverManager.getConnection(getProps().getProperty("connection.url"))) {
			try (Statement statement = connection.createStatement()) {
				ResultSet data = statement.executeQuery(query);

				while (data != null && data.next())
					result.add(User.createUser(data.getInt("id"), data.getString("login")));

			}
		} catch (SQLException e) { throw new DBException("findAllUsers failed", e); }

		return result;
	}

	public boolean insertUser(User user) throws DBException {
		String query = "INSERT INTO users (login) VALUES (?)";

		try (Connection connection = DriverManager.getConnection(getProps().getProperty("connection.url"))) {
			try (PreparedStatement statement = connection.prepareStatement(query, new int[]{1})) {

				statement.setString(1, user.getLogin());
				statement.executeUpdate();

				ResultSet rs = statement.getGeneratedKeys();
				if (rs != null && rs.next())
					user.setId(rs.getInt(1));

			}
		} catch (SQLException e) { throw new DBException("insertUser method failed", e); }
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try (Connection connection = DriverManager.getConnection(getProps().getProperty("connection.url"))) {
			connection.setAutoCommit(false);

			try (Statement statement = connection.createStatement()) {
				for (User user : users) {
					String query = String.format("DELETE FROM users WHERE login = '%s'", user.getLogin());
					int updatedRows = statement.executeUpdate(query);
					if (updatedRows == 0)
						return false;
				}
				connection.commit();
			}
		} catch (SQLException e) { throw new DBException("deleteUsers method failed", e); }
		return false;
	}

	public User getUser(String login) throws DBException {
		String query = String.format("SELECT * FROM users WHERE login = '%s'", login);

		try (Connection connection = DriverManager.getConnection(getProps().getProperty("connection.url"))) {
			try (Statement statement = connection.createStatement()) {

				ResultSet data = statement.executeQuery(query);
				data.next();
				return User.createUser(data.getInt(1), data.getString("login"));

			}
		} catch (SQLException e) { throw new DBException("getUser method failed", e); }
	}

	public Team getTeam(String name) throws DBException {
		String query = String.format("SELECT * FROM teams WHERE name = '%s'", name);

		try (Connection connection = DriverManager.getConnection(getProps().getProperty("connection.url"))) {
			try (Statement statement = connection.createStatement()) {
				ResultSet data = statement.executeQuery(query);

				data.next();
				return Team.createTeam(data.getInt("id"), data.getString("name"));

			}
		} catch (SQLException e) { throw new DBException("getTeam method failed", e); }
	}

	public List<Team> findAllTeams() throws DBException {
		String query = "SELECT * FROM teams";
		List<Team> result = new ArrayList<>();

		try (Connection connection = DriverManager.getConnection(getProps().getProperty("connection.url"))) {
			try (Statement statement = connection.createStatement()) {
				ResultSet data = statement.executeQuery(query);

				while (data != null && data.next())
					result.add(Team.createTeam(data.getString("name")));
			}
		} catch (SQLException e) { throw new DBException("findAllTeams failed", e); }

		return result;
	}

	public boolean insertTeam(Team team) throws DBException {
		String query = "INSERT INTO teams (name) VALUES (?)";

		try (Connection connection = DriverManager.getConnection(getProps().getProperty("connection.url"))) {
			try (PreparedStatement statement = connection.prepareStatement(query, new int[]{1})) {

				statement.setString(1, team.getName());
				if (statement.executeUpdate() == 0)
					return false;

				ResultSet rs = statement.getGeneratedKeys();
				if (rs != null && rs.next())
					team.setId(rs.getInt(1));

			}
		} catch (SQLException e) { throw new DBException("insertTeam failed", e); }
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		String query = "INSERT INTO users_teams VALUES (?, ?)";

		try (Connection connection = DriverManager.getConnection(getProps().getProperty("connection.url"))) {
			connection.setAutoCommit(false);

			try (PreparedStatement statement = connection.prepareStatement(query))
			{
				for (Team team : teams) {
					statement.setInt(1, user.getId());
					statement.setInt(2, team.getId());
					statement.executeUpdate();
				}
				connection.commit();
			} catch (SQLException e) { connection.rollback(); throw new DBException("setTeamsForUser method failed", e); }
		} catch (SQLException e) { throw new DBException("setTeamsForUser method failed", e); }

		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		String query =
				"SELECT teams.id AS id, teams.name AS name FROM teams\n" +
						"JOIN users_teams ON teams.id = users_teams.team_id\n" +
						"JOIN users ON users_teams.user_id = users.id\n" +
						"WHERE users.id = " + user.getId();
		List<Team> result = new ArrayList<>();

		try (Connection connection = DriverManager.getConnection(getProps().getProperty("connection.url"))) {
			try (Statement statement = connection.createStatement()) {
				ResultSet data = statement.executeQuery(query);
				while (data != null && data.next())
					result.add(Team.createTeam(data.getInt("id"), data.getString("name")));
			}
		} catch (SQLException e) { throw new DBException("getUserTeams method failed", e); }

		return result;
	}

	public boolean deleteTeam(Team team) throws DBException {
		String query = String.format("DELETE FROM teams WHERE name = '%s'", team.getName());

		try (Connection connection = DriverManager.getConnection(getProps().getProperty("connection.url"))) {
			try (Statement statement = connection.createStatement()) {
				int rowsUpdated = statement.executeUpdate(query);

				if (rowsUpdated == 0)
					return false;
			}
		} catch (SQLException e) { throw new DBException("deleteTeam method failed", e); }
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		String query = String.format("UPDATE teams SET name = '%s' WHERE id = %d", team.getName(), team.getId());

		try (Connection connection = DriverManager.getConnection(getProps().getProperty("connection.url"))) {
			try (Statement statement = connection.createStatement()) {
				int rowsUpdated = statement.executeUpdate(query);

				if (rowsUpdated == 0)
					return false;
			}
		} catch (SQLException e) { throw new DBException("updateTeam method failed", e); }
		return true;
	}

}
