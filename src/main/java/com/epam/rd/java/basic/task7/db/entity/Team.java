package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;
	private String name;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam() { return new Team(); }
	public static Team createTeam(String name) {
		Team res = new Team();
		res.setName(name);
		return res;
	}
	public static Team createTeam(int id, String name) {
		Team res = createTeam(name);
		res.setId(id);
		return res;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Team))
			return false;

		return ((Team) obj).getName().equals(name);
	}
}
