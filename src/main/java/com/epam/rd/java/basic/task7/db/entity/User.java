package com.epam.rd.java.basic.task7.db.entity;

public class User {

	private int id;
	private String login;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		User user = new User();
		user.setLogin(login);
		return user;
	}
	public static User createUser(int id, String login) {
		User user = createUser(login);
		user.setId(id);
		return user;
	}

	@Override
	public String toString() {
		return login;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof User))
			return false;

		return ((User) obj).getLogin().equals(login);
	}
}
